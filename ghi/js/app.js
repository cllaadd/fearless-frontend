window.addEventListener('DOMContentLoaded', async () => {
    // function createPlaceHolder(){
    //     return `
    //     <div class="card" aria-hidden="true">
    //     <svg class="bd-placeholder-img card-img-top" width="100%" height="180" focusable="false"><rect width="100%" height="100%" fill="#868e96">
    //         <div class="card-body">
    //             <h5 class="card-title placeholder-glow">
    //             <span class="placeholder col-6"></span>
    //             </h5>
    //             <p class="card-text placeholder-glow">
    //             <span class="placeholder col-7"></span>
    //             <span class="placeholder col-4"></span>
    //             <span class="placeholder col-4"></span>
    //             <span class="placeholder col-6"></span>
    //             <span class="placeholder col-8"></span>
    //             </p>
    //             <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
    //         </div>
    //     </div>
    //     `.repeat(6)
    // }

    // const container = document.querySelector('.container')
    // const placeHolderHtml = `
    // <h2>Upcoming conferences</h2>
    // <div class="row row-cols-3">
    // ${createPlaceHolder()}
    // </div>
    // `
    // container.innerHTML = placeHolderHtml;

    function createCard(title, description, pictureUrl, start, end, location) {
      return `
          <div class="col-sm-6 col-md-4 mb-1">
              <div class="card shadow-lg p-3 mb-5 bg-body rounded">
                  <img src="${pictureUrl}" class="card-img-top" alt="...">
                  <div class="card-body">
                      <h5 class="card-title">${title}</h5>
                      <h6 class="card-subtitle mb-2 text-muted">${location}</h5>
                      <p class="card-text">${description}</p>
                  </div>
                  <div class="card-footer">
                      ${start} - ${end}
                  </div>
              </div>
          </div>
          `;
      }

    function errorAlert(e) {
      return `
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              ${e}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                  <span aria-hidden="true">&times;</span>
          </div>
      `;
    }


  const url = 'http://localhost:8000/api/conferences/';

  try {
      const response = await fetch(url);

      if (!response.ok) {
          const e = "An error happened!"
          console.error(e)
          const html = errorAlert(e);
          const column = document.querySelector('.row');
          column.innerHTML += html;
          // Figure out what to do when the response is bad
      } else {
          const data = await response.json();

          for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const title = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const startDate = new Date(details.conference.starts);
                  const start = `${startDate.getMonth()}/${startDate.getDate()}/${startDate.getFullYear()}`
                  const endDate = new Date(details.conference.ends);
                  const end = `${endDate.getMonth()}/${endDate.getDate()}/${endDate.getFullYear()}`
                  const location = details.conference.location.name;
                  const html = createCard(title, description, pictureUrl, start, end, location);
                  const column = document.querySelector('.row');
                  column.innerHTML += html;
              }
          }
      }
  } catch (e) {
      console.error(e)
      const html = errorAlert(e);
      const error = document.querySelector('.row');
      error.innerHTML += html;

  }

  });
