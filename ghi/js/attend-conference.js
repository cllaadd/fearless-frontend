window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

try {
    const response = await fetch(url);

    if (!response.ok) {
        console.error("there has been an error")
    } else {
      const data = await response.json();
      const selectTag = document.getElementById('conference');

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

        const loadingTag = document.getElementById('loading-conference-spinner')
        loadingTag.classList.add('d-none')
        selectTag.classList.remove('d-none')
    }
        const formTag = document.getElementById('create-attendee-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                // const attendeeid = Object.fromEntries(formData).attendees;
                // const attendee_name = Object.fromEntries(formData).attendee_name;
                // const attendee_email = Object.fromEntries(formData).attendee_email;
                // const company_name = Object.fromEntries(formData).company_name;
                // let test = {}
                // test["attendee_name"] = attendee_name
                // test["attendee_email"] = attendee_email
                // test["company_name"] = company_name
                // console.log(test)
                const json = JSON.stringify(Object.fromEntries(formData));
                const attendeeUrl = 'http://localhost:8001/api/attendees/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(attendeeUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const successTag = document.getElementById('success-message')
                    formTag.classList.add('d-none')
                    successTag.classList.remove('d-none')
                    const newAttendee = await response.json()
                    console.log(newAttendee)
                }

        });
    } catch (e) {
        console.error("There has been an error")
    }
});
