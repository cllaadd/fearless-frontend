import React from 'react';


// handleNameChange = (event) => {this.setState({name:value})}

class PresentationForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            company: '',
            title: '',
            synopsis: '',
            conferences: []
          };
        this.state = {conferences: []};
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleCompanyChange = this.handleCompanyChange.bind(this);
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleSynopsisChange = this.handleSynopsisChange.bind(this);
        this.handleConferenceChange = this.handleConferenceChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }


      async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          this.setState({conferences: data.conferences});

        }
      }

      async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.conferences;
        console.log(data);


        const presentationUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            const cleared = {
            name: '',
            email: '',
            company: '',
            title: '',
            synopsis: '',
            conference: '',
            };
            this.setState(cleared);
          } }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
      }

    handleEmailChange(event) {
        const value = event.target.value;
        this.setState({email: value})
      }

    handleCompanyChange(event) {
        const value = event.target.value;
        this.setState({company: value})
      }

    handleTitleChange(event) {
        const value = event.target.value;
        this.setState({title: value})
      }

    handleSynopsisChange(event) {
        const value = event.target.value;
        this.setState({synopsis: value})
      }

    handleConferenceChange(event) {
        const value = event.target.value;
        this.setState({conference: value})
      }

    render () {
      return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={this.handleSubmit} id="create-presentation-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEmailChange} value={this.state.email} placeholder="Email" required type="text" name="email" id="email" className="form-control" />
                    <label htmlFor="starts">Email</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleCompanyChange} value={this.state.company} placeholder="Company" required type="text" name="company" id="company" className="form-control" />
                    <label htmlFor="ends">Company</label>
                  </div>
                  <div className="form-floating mb-3">
                      <input onChange={this.handleTitleChange} value={this.state.title} placeholder="Title" required type="text" name="title" id="title" className="form-control" />
                      <label htmlFor="max_presentations">Title</label>
                    </div>
                  <div className="mb-3">
                    <label htmlFor="description">Synopsis</label>
                    <input onChange={this.handleSynopsisChange} value={this.state.synopsis} placeholder="Description" required type="textarea" name="description" id="description" className="form-control" />
                  </div>
                    <div className="mb-3">
                      <select onChange={this.handleConferenceChange} required id="conference"  name="conference" className="form-select" value={this.state.conference}>
                      <option value="">Choose a conference</option>
                          {this.state.conferences.map(conference=> {
                              return (
                                  <option key = {conference.id} value={conference.id}>
                                      {conference.name}
                                      </option>
                              )
                              })};
                      </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                  </form>
                </div>
              </div>
            </div>
        );
    }
}

export default PresentationForm;
